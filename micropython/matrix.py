"""
Author: Olivier Lenoir <olivier.len02@gmail.com>
Created: 2021-01-21 22:03:39
License: MIT, Copyright (c) 2021-2022, 2024 Olivier Lenoir
Project: Matrix, MicroPython
Description:
"""


def add(_a, _b):
    """Addition"""
    return _a + _b


def sub(_a, _b):
    """Subtraction"""
    return _a - _b


def mul(_a, _b):
    """Multiplication"""
    return _a * _b


def square(_a):
    """Square"""
    return _a * _a


def equal(_a, _b):
    """Equal"""
    return _a == _b


class Matrix:
    """Matrix"""

    def __init__(self, mat):
        """Matrix A, list of list"""
        self.mat = mat

    def dim(self):
        """Matrix dimension m rows by n cols"""
        return len(self.mat), len(self.mat[0])

    def map(self, func):
        """Map function to all elements of the matrix"""
        return Matrix([list(map(func, m)) for m in self.mat])

    def sumsquare(self):
        """Sum square of elements"""
        return sum(sum(m) for m in self.map(square).mat)

    def meansquare(self):
        """Mean square of elements"""
        return self.sumsquare() / mul(*self.dim())

    def transpose(self):
        """Matrix transpose"""
        return Matrix(list(zip(*self.mat)))

    def minor(self, row, col):
        """Matrix minor"""
        mat = self.mat.copy()
        mat.pop(row)
        mat = list(zip(*mat))
        mat.pop(col)
        return Matrix(list(zip(*mat)))

    def det(self):
        """Matrix determinant
        https://en.wikipedia.org/wiki/Laplace_expansion"""
        def _det(_m):
            if len(_m) == 1:
                return _m[0][0]

            _t = 0
            for _c, _e in enumerate(_m[0]):
                _t += (1 if _c % 2 == 0 else -1) * _e * _det([x[:_c] + x[_c + 1:] for x in _m[1:]])
            return _t

        return _det(self.mat)

    def adj(self):
        """Adjugate matrix"""
        if self.dim() == (2, 2):
#            _a, _b, _c, _d = *self.mat[0], *self.mat[1]
            _a, _b = self.mat[0]
            _c, _d = self.mat[1]
            return Matrix([[_d, -_b], [-_c, _a]])
        row, col = self.dim()
        mat = [[self.minor(r, c).det() * (-1) ** (r + c) for r in range(row)] for c in range(col)]
        return Matrix(mat)

    def __str__(self):
        return f'{self.mat}'

    def __repr__(self):
        return f'{self.mat}'

#    def __eq__(self, other):
#        pass
#
#    def __ne__(self, other):
#        return not self.__eq__(other)

    def __invert__(self):
        """Matrix inverse
        ~A"""
        return (1 / self.det()) * self.adj()

    def __neg__(self):
        """- A"""
        return self.map(lambda e: -e)

    def __pos__(self):
        """+ A"""
        return self.map(lambda e: e)

    def __add__(self, other):
        """A + B"""
        if not isinstance(other, Matrix):
            return Matrix([[_n + other for _n in _m] for _m in self.mat])
        return Matrix([list(map(add, *ab)) for ab in zip(self.mat, other.mat)])

    def __radd__(self, other):
        """B + A"""
        return self.__add__(other)

    def __iadd__(self, other):
        """A += B"""
        self.mat = self + other
        return self.mat

    def __sub__(self, other):
        """A - B"""
        if not isinstance(other, Matrix):
            return Matrix([[_n - other for _n in _m] for _m in self.mat])
        return Matrix([list(map(sub, *ab)) for ab in zip(self.mat, other.mat)])

    def __rsub__(self, other):
        """B - A"""
        return self.__sub__(other)

    def __isub__(self, other):
        """A -= B"""
        self.mat = self - other
        return self.mat

    def __mul__(self, other):
        """Scalar multiplication A * other"""
        if not isinstance(other, Matrix):
            return Matrix([[_n * other for _n in _m] for _m in self.mat])
        return Matrix([list(map(mul, *ab)) for ab in zip(self.mat, other.mat)])

    def __rmul__(self, other):
        """Scalar multiplication other * A"""
        return self.__mul__(other)

    def __imul__(self, other):
        """Scalar multiplication A *= other"""
        self.mat = self * other
        return self.mat

    def __matmul__(self, other):
        """A @ B"""
        return Matrix(
            [[sum(map(mul, _a, _b)) for _b in zip(*other.mat)]
                for _a in self.mat])

    def __imatmul__(self, other):
        """A @= B"""
        self.mat = self @ other
        return self.mat
