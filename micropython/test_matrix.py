"""
Author: Olivier Lenoir <olivier.len02@gmail.com>
Created: 2021-01-21 22:04:10
License: MIT, Copyright (c) 2021-2022, 2024 Olivier Lenoir
Project: Test Matrix, MicroPython
Description:
"""


from math import exp
from matrix import Matrix


a = Matrix([[1, 2, 3], [4, 5, 6]])
b = Matrix([[10, 11], [20, 21], [30, 31]])
c = Matrix([[-23, 3, 8], [-13, 17, -21]])
d = Matrix([[32, 17], [-3, 11]])
e = Matrix([[9, 8, 7], [6, 5, 4]])
A = Matrix([[1, 1, 1, 1], [2, 3, 0, -1], [-3, 4, 1, 2], [1, 2, -1, 1]])
B = Matrix([[13], [-1], [10], [1]])


def sigmoid(_a, deriv=False):
    """Sigmoid"""
    if deriv:
        return _a * (1 - _a)
    return 1 / (1 + exp(-_a))


print('a =', a)
print('b =', b)
print('c =', c)
print('d =', d)
print('e =', e)

print('a.dim() =', a.dim())
print('b.dim() =', b.dim())
print('c.dim() =', c.dim())
print('d.dim() =', d.dim())
print('e.dim() =', e.dim())

print('d.det() =', d.det())
print('~d =', ~d)

print('a + c =', a + c)
print('a - c =', a - c)

print('a + 2 =', a + 2)
print('2 + a =', 2 + a)

print('a - 3 =', a - 3)
print('3 - a =', 3 - a)

print('a * 2 =', a * 2)
print('2 * a =', 2 * a)

print('a * c =', a * c)

e *= 3
print('e *= 3 :', e)

print('a @ b =', a @ b)
print('(a @ b).det() =', (a @ b).det())

print('b @ a =', b @ a)
print('(b @ a).det() =', (b @ a).det())

print('a @ b + d =', a @ b + d)
print('(a @ b + d).det() =', (a @ b + d).det())

print('(a @ b + c @ b).det() =', (a @ b + c @ b).det())

a += c
print('a += c :', a)

a -= c
print('a -= c :', a)

a += 2
print('a += 2 :', a)

a += -2
print('a += -2 :', a)

a -= 3
print('a -= 3 :', a)

a -= -3
print('a -= -3 :', a)

a @= b
print('a @= b :', a)

print('c @ b + a =', c @ b + a)

print('c @ b + a - d =', c @ b + a - d)

print('b.map(sigmoid) =', b.map(sigmoid))
print(
    'b.map(lambda m: sigmoid(m, deriv=True)) =',
    b.map(lambda m: sigmoid(m, deriv=True))
    )

e = c @ b + a - d
print('e = c @ b + a - d = ', e)
print('e.map(sigmoid) =', e.map(sigmoid))
print('(c @ b + a - d).map(sigmoid) =', (c @ b + a - d).map(sigmoid))

print('Solve linear equation Ax = B')
print('A =', A)
print('B =', B)
print('~A @ B =', ~A @ B)
print('(~A @ B).map(round) =', (~A @ B).map(round))
